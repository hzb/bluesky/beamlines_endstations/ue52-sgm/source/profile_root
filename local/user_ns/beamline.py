from bessyii_devices.ring import Ring
from bessyii_devices.au import AU1UE52SGM
from bessyii_devices.pinhole import PinholeUE52SGM
#from bessyii_devices.m3_m4_mirrors import SMUMetrixs
from bessyii_devices.m3_m4_mirrors import SMUUE52SGM
from bessyii_devices.undulator import UndulatorUE52
from bessyii_devices.pgm import SGMUE52
from bessyii_devices.keithley import Keithley6517, Keithley6514

from bessyii_devices.lock import Lock

# standard magics
from bluesky.magics import BlueskyMagics
get_ipython().register_magics(BlueskyMagics)

# If magics for the beamline are defined import them like this
# (example taken from AQUARIUS beamline)
#from aquariustools.plans.magics_aquarius import BlueskyMagicsAQUARIUS
#get_ipython().register_magics(BlueskyMagicsAQUARIUS)


#Beamline Lock
lock = Lock("UE52SGMEL:LOCK00:BEAMLINE:",name="lock")

# Ring
bessy2 = Ring('MDIZ3T5G:', name='ring')

# Keithleys
kth01 = Keithley6517('UE52SGMOL:' + 'Keithley01:',  name='Keithley01',read_attrs=['readback'])
kth02 = Keithley6514('UE52SGMOL:' + 'Keithley02:',  name='Keithley02',read_attrs=['readback'])

# sgm
sgm = SGMUE52('ue521sgm1:', name='sgm')

# Undulators
ue52 = UndulatorUE52('UE52ID5R:', name='ue52')

# Apertures
au1 = AU1UE52SGM('OMSX_:h090000', name = 'au1')

# Pinhole
ph = PinholeUE52SGM('OMSX_:h090100', name = 'ph')

# Mirrors
#m1 = SMUMetrixs('SMU52_1:', name='m1')
m1 = SMUUE52SGM('SMUYU109L:', name='m1')
