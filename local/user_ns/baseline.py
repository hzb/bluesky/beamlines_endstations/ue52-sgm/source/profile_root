from bluesky.preprocessors import SupplementalData
from .base import *
from .beamline import *

sd = SupplementalData()
RE.preprocessors.append(sd)


# here all the elements that should be present in the baseline
# separated by a "",""
sd.baseline = [bessy2.current, bessy2.lifetime,
               ue52.id_control, ue52.gap, ue52.harmonic_01_eV, ue52.shift,
               au1.top, au1.bottom, au1.right, au1.left,
               m1.tx, m1.rx, m1.ry, m1.rz,
               sgm.en, sgm.diff_order, sgm.cff, sgm.grating, sgm.slitwidth,
               ph.h,ph.v
               ]


