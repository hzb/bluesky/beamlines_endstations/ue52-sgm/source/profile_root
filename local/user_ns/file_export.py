import event_model
from pathlib import Path
from event_model import RunRouter

import suitcase.specfile
from suitcase.specfile import Serializer

from .base import *

# Export everything to individual files
# change beamline_name to the beamline name
beamline_name = 'UE52SGM' 
def spec_single_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory,file_prefix=beamline_name+'_'+str(start_doc['scan_id']))

    return [serializer], []

# Export everything to a single file
# https://github.com/NSLS-II-CHX/profile_collection/blob/15202d1019def2a0132f0bdb74f54cb5450e8117/startup/99-bluesky.py#L258-L299
 
def spec_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory, file_prefix=spec_factory.file_prefix, flush=True)
    return [serializer], []


def new_spec_file(name):
    """
    set new specfile name:
    - path is fixed at /home/xf11id/specfiles/
    - name= xyz .spec will be added automatically
    calling sequence: new_spec_file(name='xyz')
    """
    spec_factory.file_prefix = name


# Configure data export
#from file_export import spec_factory, spec_single_factory, new_spec_file

# Initialize the filename to today's date.
import time

spec_factory.file_prefix = beamline_name+'_spec_'+time.strftime('%Y')
spec_factory.directory = "/home/psadmin/bluesky/data/"

from event_model import RunRouter
rr = RunRouter([spec_factory])

RE.subscribe(rr)

