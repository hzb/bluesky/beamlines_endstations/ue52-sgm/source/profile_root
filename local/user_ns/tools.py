# in this file all the imports useful to the beamline

# imports from beamlinetools
from beamlinetools.plans.keithley_scripts import kth_range, kth_voltage
from beamlinetools.plans.ID_controls import IDon, IDoff, SetHarmonic, SetTable



#imports from bessyii
from .base import *
from .beamline import *
from .plans import *
"""
RE._silent_det = [bessy2.current, 
                 kth01, kth02, 
                 ue52.gap, ue52.shift,
                 au1.top, au1.bottom, au1.right, au1.left,
                 m1.tx, m1.rx, m1.ry, m1.rz,
                 sgm.en, sgm.diff_order, sgm.cff, sgm.grating, sgm.slitwidth,
                 ph.h,ph.v
                 ]
"""
# RE._silent_det = [bessy2.current, 
#                   kth01, kth02, 
#                   sgm.diff_order, sgm.cff, sgm.grating, sgm.slitwidth,
#                   ph.h,ph.v
#                   ]

from bessyii.plans.n2fit import N2_fit
N2fit_class = N2_fit(db)
fit_n2 = N2fit_class.fit_n2

from bessyii.helpers import Helpers
helpers = Helpers(db)
comp = helpers.comp


from beamlinetools.plans.magics_beamline import BlueskyMagicsBEAMLINE
get_ipython().register_magics(BlueskyMagicsBEAMLINE)


from bessyii.load_script import simple_load

SL = simple_load(user_script_location='/home/psadmin/bluesky/user_scripts/')
load_user_script = SL.load_script
