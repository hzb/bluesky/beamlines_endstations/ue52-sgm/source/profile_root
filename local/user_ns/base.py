
from os.path import expanduser

from bessyii.RunEngine.RunEngineBessy import RunEngineBessy
RE = RunEngineBessy({})


from bluesky.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()

# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)

## Put plotting into another process

#from bluesky.callbacks.zmq import Publisher
#publisher = Publisher('localhost:5577')
#RE.subscribe(publisher)


# Database definition, change catalog_name for the actual
# name of the database
import databroker
db = databroker.catalog["UE52SGM"]
RE.subscribe(db.v1.insert)

# Temporary database, once mongo is installed and a database created use
 # comment the following lines 
#from databroker import Broker
#db = Broker.named('temp')
#RE.subscribe(db.insert)


# If you need debug messages from the RE then uncomment this
#from bluesky.utils import ts_msg_hook
#RE.msg_hook = ts_msg_hook


# DO WE NEED THIS AT UE52SGM?
# Configure persistence between sessions of metadata
# change beamline_name to the name of the beamline
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()
RE.md = PersistentDict('/home/psadmin/bluesky/persistence/beamline')


import databroker.core
handler_registry = databroker.core.discover_handlers()

# import matplotlib
import matplotlib.pyplot as plt

